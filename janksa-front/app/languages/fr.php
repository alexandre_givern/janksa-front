<?php

$messages = array();

// Login and signin page
$messages = array_merge($messages, array(
    'Email' => 'Votre adresse mail',
    'Confirm password' => 'Confirmez votre mot de passe',
    'Password' => 'Votre mot de passe',
    'Login' => 'Vous connecter',
    'Register' => 'Vous enregistrer',
    'Username' => 'Votre pseudo',
    'Sign up using this form' => 'Formulaire d\'inscription',
    'You are regristred and connected, welcome' => 'Vous êtes enregistré et connecté, bievenue',
    'An error occured during the registration' => 'Une erreur est survenue durant votre inscription',
    'You are connected' => 'Vous êtes connecté',
    'The password is incorrect' => 'Le mot de passe est incorrect',
    'There is a problem with your account, contact us : contact@janksa.com' => 'Il y a un problème avec votre compte, contactez nous : contact@janksa.com',
    'We don\'t find an account with this email' => 'Nous ne trouvons pas de compte avec cet email',
    'Username already exists' => 'Ce pseudo existe déjà',
    'Username is required' => 'Le pseudo est requis',
    'Password and password\'s confirmation are different' => 'Le mot de passe de confirmation est différent',
    'Password and password\'s confirmation are required' => 'Le mot de passe est requis',
    'Email already exists' => 'Cet adresse mail existe déjà',
    'Email format is incorrect' => 'Le format de l\'adresse mail est incorrect',
    'Email is required' => 'L\'adresse mail est requis',
    'This member can\'t be edited by you' => 'Ce membre ne peut pas être éditer par vous',
    'This member doesn\'t exist' => 'Ce membre n\'existe pas',
    'The member\'s id is required' => 'L\'id du membre est requis',
));

// Header
$messages = array_merge($messages, array(
    'Home' => 'Accueil',
    'Projects' => 'Les projets',
    'Articles' => 'Nos articles',
    'Signin' => 'S\'inscrire',
    'Signup / Signin' => ' Se connecter / S\'inscrire',
    'Signup' => 'Se connecter',
    'Notifications' => 'Notifications',
    'Inbox' => 'Messagerie',
));

// Footer
$messages = array_merge($messages, array(
    'Term of use' => 'Conditions générales d\'utilisation',
    'Privacy policy' => 'Politique de confidentialité',
    'Choose your language' => 'Choisissez votre langage',
));

// Common
$messages = array_merge($messages, array(
    'Edit' => 'Modifier',
    'Do you have an incredible poject ? Publish it now !' => 'Avez vous un incroyable projet ? Publiez le maintenant !',
    'Publish it' => 'Publiez votre projet',
    'Create' => 'Créer',
    'Account' => 'Profil',
));

// Home page
$messages = array_merge($messages, array(
    'Together,  let\'s  participate in the innovations of tomorrow' => 'Ensemble, participons aux innovations de demain',
    'Create a project' => 'Créer un projet',
    'Discover the projects' => 'Découvrir les projets',
));

// project page
$messages = array_merge($messages, array(
    'Create your project' => 'Création de votre projet',
    'Edit your project' => 'Modification de votre projet',
    'Presentation' => 'Présentation',
    'News' => 'News',
    'Recruitment' => 'Recrutement',
    'Comments' => 'Commentaires',
    'Contact' => 'Contact',
    'You must to have an account to comment this project' => 'Vous devez avoir un compte pour commenter ce projet',
    'You must to have an account to see the contact' => 'Vous devez avoir un compte pour voir les informations de contact',
    'There are no jobs available yet' => 'Il n\'y a aucun recrutements en cours',
    'There are no news yet' => 'Il n\'y a pas de news pour le moment',
    'There are no comments yet' => 'Il n\'y a pas de commentaire, soyez le premier',
    'Post a news' => 'Publier une news',
    'Post a recruitment' => 'Publier une annonce',
    'Comment' => 'Commenter',
    'Name' => 'Nom du projet',
    'Description' => 'Description du projet',
    'Image' => 'Image du projet',
    'Discover all the projects' => 'Découvrez tous les projets',
    'FOLLOW' => 'S\'ABONNER',
    'UNFOLLOW' => 'SE DESABONNER',
    'FOLLOWERS' => 'ABONNES',
    'The image is required' => 'L\'image est obligatoire'
));

// project page
$messages = array_merge($messages, array(
    'Discover all our articles' => 'Découvrez tous nos articles',
));

// Inbox page
$messages = array_merge($messages, array(
    'To contact' => 'Contacter',
    'Your inbox is empty' => 'Vous avez aucun message',
));

<?php
use Phalcon\Mvc\Controller;
class IndexController extends Controller
{

    /**
     * Home page
     * Get the latest project with the API
     */
    public function indexAction()
    {
        try {
            $oResponse = $this->oProvider->get(
                'projects/' . $this->sApiToken . '?limit={"min":0,"max":6}&sort_by={"name":"date","type":"DESC"}',
                array()
            );
            $aProjects = json_decode($oResponse->body, true);
        }
        catch (Exception $e)
        {
            $aProjects = array();
        }

        try {
            $oResponse = $this->oProvider->get(
                'articles/' . $this->sApiToken . '?limit={"min":0,"max":6}&sort_by={"name":"date","type":"DESC"}',
                array()
            );
            $aArticles = json_decode($oResponse->body, true);
        }
        catch (Exception $e)
        {
            $aArticles = array();
        }

        $this->view->aProjects = $aProjects;
        $this->view->aArticles = $aArticles;
    }

    /**
     *
     */
    public function privacyAction()
    {

    }

    /**
     *
     */
    public function termsAction()
    {

    }

    /**
     * List of the project
     * Get all the projects ordered by the most recent creation
     */
    public function projectsAction()
    {
        try {
            $oResponse = $this->oProvider->get(
                'projects/' . $this->sApiToken . '?sort_by={"name":"date","type":"DESC"}',
                array()
            );
            $aResponse = json_decode($oResponse->body, true);
        }
        catch (Exception $e)
        {
            $aResponse = array();
        }

        $this->view->aProjects = $aResponse;
    }

    /**
     * List of the articles
     * Get all the articles ordered by the most recent creation
     */
    public function articlesAction()
    {
        try {
            $oResponse = $this->oProvider->get(
                'articles/' . $this->sApiToken . '?sort_by={"name":"date","type":"DESC"}',
                array()
            );
            $aResponse = json_decode($oResponse->body, true);
        }
        catch (Exception $e)
        {
            $aResponse = array();
        }

        $this->view->aArticles = $aResponse;
    }
}

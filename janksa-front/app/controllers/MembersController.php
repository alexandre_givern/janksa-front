<?php
use Phalcon\Mvc\Controller;
use Phalcon\Mvc\View;
use Phalcon\Http\Client\Request;


class MembersController extends Controller
{

    /**
     * Popin to signin and signup
     * We return only the content (no header and no footer)
     */
    public function signAction()
    {
        $this->view->setRenderLevel(View::LEVEL_ACTION_VIEW);
    }


    public function indexAction()
    {

    }

    /**
     * Update/insert members on the database
     * If member's password changed, the function compare with a password
     * confirmation variable. If it's same, password is hashed.
     */
    public function registerAction()
    {
        $aParameters = $this->request->getPost();
        $aResponse = array();

        try
        {
            if (isset($aParameters['memberId']))
            {
                MembersModel::updateMember($aParameters);
                $aResponse[] = 'Member updated';
            }
            else
            {
                $iMemberId = MembersModel::insertMember($aParameters);
                if (!empty($iMemberId))
                {
                    $aResponse[] = 'Member created';
                    $this->session->set('member_id', $iMemberId);
                    $this->session->set('member_type', 'guest');
                    $aResponse['success'] = $this->translation->_('You are regristred and connected, welcome');
                }
                else
                {
                    $aResponse['error'] = $this->translation->_('An error occured during the registration');
                }
            }
        }
        catch(Exception $e)
        {
            $aResponse['error'] = $this->translation->_($e->getMessage());
        }

        return json_encode($aResponse);
    }

    /**
     * Create a session with the member id when email and password match a id
     * member.
     *
     */
    public function loginAction()
    {
        $sEmail = $this->request->get('email');
        $sPassword = $this->request->get('password');

        $aResponse = array();

        if (!empty($sEmail) && !empty($sPassword))
        {
            $oMembers = Members::findFirst(array(
                'email = ?0',
                'bind' => array($sEmail)
            ));


            if ($oMembers !== false)
            {
                $aMembers = $oMembers->toArray();

                $oMemberInformations = MemberInformations::findFirst(array(
                    'memberId = ?0',
                    'bind' => array($aMembers['memberId'])
                ));

                if ($oMemberInformations !== false)
                {
                    $aMembers['informations'] = $oMemberInformations->toArray();

                    if ($this->security->checkHash($sPassword, $aMembers['informations']['password']))
                    {
                        $this->session->set('member_id', $aMembers['memberId']);
                        if ($aMembers['memberId'] == 1)
                        {
                            $this->session->set('member_type', 'admin');
                        }
                        else
                        {
                            $this->session->set('member_type', 'guest');
                        }
                        $aResponse['success'] = $this->translation->_('You are connected');
                    }
                    else
                    {
                        $aResponse['error'] = $this->translation->_('The password is incorrect');
                    }
                }
                else
                {
                    $aResponse['error'] = $this->translation->_('There is a problem with your account, contact us : contact@janksa.com');
                }
            }
            else
            {
                $aResponse['error'] =  $this->translation->_('We don\'t find an account with this email');
            }
        }

        return json_encode($aResponse);
    }

    /**
     * Remove the member id from the session
     */
    public function logoutAction()
    {
        $this->session->remove('member_id');
        $this->response->redirect('');
    }
}

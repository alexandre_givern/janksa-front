<?php
use Phalcon\Mvc\Controller;
use Phalcon\Http\Client\Request;
use Phalcon\Mvc\View;

class articlesController extends Controller
{

    /**
     * Main article page
     * Get all the information about a article (the article id must to be on the header)
     *
     */
    public function indexAction()
    {
        if ($this->request->has('id'))
        {
            $aParameters = array();

            try
            {
                $oResponse = $this->oProvider->get(
                    'articles/' . $this->sApiToken . '/' . $this->request->get('id'),
                    $aParameters
                );

                $aResponse = json_decode($oResponse->body, true);
            }
            catch (Exception $e)
            {}

            if(!empty($aResponse))
            {
                $this->view->aArticle = $aResponse;
            }
            else
            {
                $this->response->redirect('');
            }
        }
        else
        {
            $this->response->redirect('');
        }
    }

    /**
     * Insert a article on the database with the API. The member is redirected
     * to the index of the new article
     */
    public function createAction()
    {
        if ($this->request->isPost() && $this->session->get('member_type') == 'admin')
        {
            $aRequestBody = $this->request->getPost();
            $aParameters = array();

            if (!empty($aRequestBody['name']) && $this->request->hasFiles()
                && !empty($aRequestBody['description']))
            {

                foreach ($this->request->getUploadedFiles() as $oFile) {
                    if ($oFile->isUploadedFile())
                    {
                        $sFileName = time() . '.' . $oFile->getExtension();

                        $sResult = $this->janksa_tools->uploadImages(
                            $oFile,
                            $this->session->get('member_id'),
                            $sFileName,
                            $this->aConfigurations['aws']['image_s3']
                        );
                        $aParameters['picture'] = $sResult;
                    }
                }
                $aParameters['name'] = $aRequestBody['name'];
                $aParameters['description'] = $aRequestBody['description'];

                try
                {
                    $this->oProvider->header->set('Content-Type', 'application/json');
                    $oResponse = $this->oProvider->post(
                        'articles/' . $this->sApiToken,
                        json_encode($aParameters)
                    );

                    $sContentLocation = $oResponse->header->get('Content-Location');
                }
                catch (Exception $e)
                {
                }

                if (!empty($sContentLocation))
                {
                    $this->response->redirect(str_replace('articles/', 'articles?id=', $sContentLocation));
                }
            }
        }
    }

    /**
     * Get all the information about the article to display a form. If it's a post
     * method, the api is called to update the article in the database and the user
     * is redirected to the article index page.
     */
    public function editAction()
    {
        if ($this->request->isPost())
        {
            $aRequestBody = $this->request->getPost();
            $aParameters = array();

            if (!empty($aRequestBody['id']) && $this->session->has('member_id') && $this->session->get('member_type') == 'admin')
            {

                $oResponse = $this->oProvider->get(
                    'articles/' . $this->sApiToken . '/' . $aRequestBody['id'],
                    $aParameters
                );

                $aResponse = json_decode($oResponse->body, true);
                if(!empty($aResponse))
                {
                    if ($this->request->hasFiles() == true) {
                        foreach ($this->request->getUploadedFiles() as $oFile) {
                            if ($oFile->isUploadedFile())
                            {
                                $sFileName = time() . '.' . $oFile->getExtension();

                                $sResult = $this->janksa_tools->uploadImages(
                                    $oFile,
                                    $this->session->get('member_id'),
                                    $sFileName,
                                    $this->aConfigurations['aws']['image_s3']
                                );
                                $aParameters['picture'] = $sResult;
                            }
                            else
                            {
                                $aParameters['picture'] = $aRequestBody['picture'];
                            }
                        }
                    }

                    $aParameters['name'] = $aRequestBody['name'];
                    $aParameters['description'] = $aRequestBody['description'];

                    $this->oProvider->header->set('Content-Type', 'application/json');
                    $oResponse = $this->oProvider->put(
                        'articles/' . $this->sApiToken . '/' . $aRequestBody['id'],
                        json_encode($aParameters)
                    );

                    $this->response->redirect('articles?id=' . $aRequestBody['id']);
                }
            }
        }
        else if ($this->request->get('id'))
        {
            if ($this->session->has('member_id') && $this->session->get('member_type') == 'admin')
            {
                $aParameters = array();

                $oResponse = $this->oProvider->get(
                    'articles/' . $this->sApiToken . '/' . $this->request->get('id'),
                    $aParameters
                );

                $aResponse = json_decode($oResponse->body, true);

                if(!empty($aResponse))
                {
                    $this->view->aArticle = $aResponse;
                }
                else
                {
                    $this->response->redirect('');
                }
            }
            else
            {
                $this->response->redirect('articles?id=' . $this->request->get('id'));
            }
        }
        else
        {
            $this->response->redirect('');
        }
    }

    /**
     * Uplaod a image on AWS S3. the image is inserted on the member folder.
     * Imgix modified the url to return a responsive url
     */
    public function uploadImageAction()
    {
        $this->view->setRenderLevel(
            View::LEVEL_NO_RENDER
        );
        $aRequestBody = $this->request->getPost();
        if ($this->request->isPost())
        {
            $aRequestBody = $this->request->getPost();
            $aParameters = array();

            if ($this->request->hasFiles())
            {
                foreach ($this->request->getUploadedFiles() as $oFile)
                {
                    if ($oFile->isUploadedFile())
                    {
                        $sFileName = time() . '.' . $oFile->getExtension();

                        $sResult = $this->janksa_tools->uploadImages(
                            $oFile,
                            $this->session->get('member_id'),
                            $sFileName,
                            $this->aConfigurations['aws']['image_s3']
                        );

                        if (!empty($sResult))
                        {
                            $array = array(
                                'url' => $this->janksa_tools->getImageUrl($sResult, array('w' => 945, 'fit' => 'max', 'auto' => 'format')),
                                'id' => time()
                            );
                            echo stripslashes(json_encode($array));
                        }
                    }
                }
            }
        }
    }
}

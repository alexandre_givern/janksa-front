<?php
use Phalcon\Mvc\Controller;
use Phalcon\Http\Client\Request;
use Phalcon\Mvc\View;

class ProjectsController extends Controller
{

    /**
     * Main project page
     * Get all the information about a project (the project id must to be on the header)
     *
     */
    public function indexAction()
    {

        if ($this->request->has('id'))
        {
            $aParameters = array();

            try
            {

                $oResponse = $this->oProvider->get(
                    'projects/' . $this->sApiToken . '/' . $this->request->get('id'),
                    $aParameters
                );

                $aResponse = json_decode($oResponse->body, true);
            }
            catch (Exception $e)
            {}

            if(!empty($aResponse))
            {
                if (!empty($aResponse['comments']))
                {
                    for ($i = 0, $c = count($aResponse['comments']); $i < $c; $i++)
                    {
                        $aParameters = array();
                        $aParameters['memberId'] = $aResponse['comments'][$i]['member'];
                        $aMember = MembersModel::getMember($aParameters);
                        $aResponse['comments'][$i]['member_username'] = $aMember['username'];
                    }
                }

                if (!empty($aResponse['followers']) && $this->session->has('member_id'))
                {
                    for ($i = 0, $c = count($aResponse['followers']); $i < $c; $i++)
                    {

                        if ($aResponse['followers'][$i]['member'] == $this->session->get('member_id'))
                        {
                            $this->view->iFollowType = $aResponse['followers'][$i]['type'];
                            break;
                        }
                    }
                }

                $this->view->aProject = $aResponse;
            }
            else
            {
                $this->response->redirect('');
            }
        }
        else
        {
            $this->response->redirect('');
        }
    }

    /**
     * Insert a project on the database with the API. The member is redirectded
     * to the index of his project
     */
    public function createAction()
    {
        if ($this->request->isPost() && $this->session->has('member_id'))
        {
            $aRequestBody = $this->request->getPost();
            $aParameters = array();

            if (!empty($aRequestBody['name']) && $this->request->hasFiles()
                && !empty($aRequestBody['description']))
            {
                foreach ($this->request->getUploadedFiles() as $oFile) {
                    if ($oFile->isUploadedFile())
                    {
                        $sFileName = time() . '.' . $oFile->getExtension();

                        $sResult = $this->janksa_tools->uploadImages(
                            $oFile,
                            $this->session->get('member_id'),
                            $sFileName,
                            $this->aConfigurations['aws']['image_s3']
                        );
                        $aParameters['picture'] = $sResult;
                    }
                }
                if (!empty($aParameters['picture']))
                {
                    $aParameters['name'] = htmlspecialchars($aRequestBody['name']);
                    $aParameters['member'] = $this->session->get('member_id');
                    $aParameters['description'] = $aRequestBody['description'];


                    try
                    {
                        $this->oProvider->header->set('Content-Type', 'application/json');
                        $oResponse = $this->oProvider->post(
                            'projects/' . $this->sApiToken,
                            json_encode($aParameters)
                        );

                        $sContentLocation = $oResponse->header->get('Content-Location');
                    }
                    catch (Exception $e)
                    {
                    }

                    if (!empty($sContentLocation))
                    {
                        $this->response->redirect(str_replace('projects/', 'projects?id=', $sContentLocation));
                    }
                }
                else
                {
                    $this->view->aAlert = array('alert' => $this->translation->_('The image is required'));
                }
            }
            else
            {
            }
        }
        else
        {
            try
            {
                $sSampleDescription = $this->view->getPartial('projects/sample_' . $this->sLanguage);
            }
            catch (Exception $e)
            {
                $sSampleDescription = '';
            }
            $this->view->sSampleDescription = $sSampleDescription;
        }
    }

    /**
     * Get all the information about the project to display a form. If it's a post
     * method, the api is called to update the project in the database and the user
     * is redirected to the project index page.
     *
     * The post action is used to update the project. It's called to psot a news or form
     * directly from the project index page.
     */
    public function editAction()
    {
        if ($this->request->isPost())
        {
            $aRequestBody = $this->request->getPost();
            $aParameters = array();

            if (!empty($aRequestBody['id']) && $this->session->has('member_id'))
            {

                $oResponse = $this->oProvider->get(
                    'projects/' . $this->sApiToken . '/' . $aRequestBody['id'],
                    $aParameters
                );

                $aResponse = json_decode($oResponse->body, true);
                if(!empty($aResponse) && ($aResponse['member'] == $this->session->get('member_id') || $this->session->get('member_type') == 'admin'))
                {
                    if ($this->request->hasFiles() == true) {
                        foreach ($this->request->getUploadedFiles() as $oFile) {
                            if ($oFile->isUploadedFile())
                            {
                                $sFileName = time() . '.' . $oFile->getExtension();

                                $sResult = $this->janksa_tools->uploadImages(
                                    $oFile,
                                    $this->session->get('member_id'),
                                    $sFileName,
                                    $this->aConfigurations['aws']['image_s3']
                                );
                                $aParameters['picture'] = $sResult;
                            }
                            else
                            {
                                $aParameters['picture'] = $aRequestBody['picture'];
                            }
                        }
                    }

                    if (!empty($aRequestBody['name']) && !empty($aRequestBody['description']))
                    {
                        $aParameters['name'] = htmlspecialchars($aRequestBody['name']);
                        $aParameters['description'] = $aRequestBody['description'];
                    }

                    if (!empty($aRequestBody['recruitments']))
                    {
                        // Recruitments must to be a list of recruitments
                        if (!isset($aRequestBody['recruitments'][0]))
                        {
                            $aRequestBody['recruitments'] = array($aRequestBody['recruitments']);
                        }

                        $aParameters['recruitments'] = $aRequestBody['recruitments'];
                    }

                    if (!empty($aRequestBody['news']))
                    {
                        $aParameters['news'] = $aRequestBody['news'];
                    }

                    $this->oProvider->header->set('Content-Type', 'application/json');
                    $oResponse = $this->oProvider->put(
                        'projects/' . $this->sApiToken . '/' . $aRequestBody['id'],
                        json_encode($aParameters)
                    );

                    $this->response->redirect('projects?id=' . $aRequestBody['id']);
                }
            }
        }
        else if ($this->request->get('id'))
        {
            if ($this->session->has('member_id'))
            {
                $aParameters = array();

                $oResponse = $this->oProvider->get(
                    'projects/' . $this->sApiToken . '/' . $this->request->get('id'),
                    $aParameters
                );

                $aResponse = json_decode($oResponse->body, true);

                if(!empty($aResponse) && ($aResponse['member'] == $this->session->get('member_id') || $this->session->get('member_type') == 'admin'))
                {
                    $this->view->aProjects = $aResponse;
                }
                else
                {
                    $this->response->redirect('');
                }
            }
            else
            {
                $this->response->redirect('projects?id=' . $this->request->get('id'));
            }
        }
        else
        {
            $this->response->redirect('');
        }
    }

    public function commentAction()
    {
        if ($this->request->isPost())
        {
            $aRequestBody = $this->request->getPost();
            $aParameters = array();

            if (!empty($aRequestBody['project']) && !empty($aRequestBody['comment']) && $this->session->has('member_id'))
            {
                $aParameters['member'] = $this->session->get('member_id');
                $aParameters['comment'] = $aRequestBody['comment'];

                $this->oProvider->header->set('Content-Type', 'application/json');
                $oResponse = $this->oProvider->put(
                    'projects/' . $this->sApiToken . '/' . $aRequestBody['project'],
                    json_encode($aParameters)
                );

                $this->response->redirect('projects?id=' . $aRequestBody['project']);
            }
        }
    }

    public function followAction()
    {
        if ($this->request->isPost())
        {
            $aRequestBody = $this->request->getPost();
            $aParameters = array();

            if (!empty($aRequestBody['project']) && $this->session->has('member_id') && !empty($aRequestBody['type'])) {
                if ($aRequestBody['type'] == 3)
                {
                    $aParameters['followers']['member'] = $this->session->get('member_id');
                    $aParameters['followers']['type'] = $aRequestBody['type'];
                    if ($aRequestBody['put'])
                    {
                        $aParameters['followers']['put'] = true;
                    }

                    $this->oProvider->header->set('Content-Type', 'application/json');
                    $oResponse = $this->oProvider->put(
                        'projects/' . $this->sApiToken . '/' . $aRequestBody['project'],
                        json_encode($aParameters)
                    );
                    $this->response->redirect('projects?id=' . $aRequestBody['project']);
                }
                else
                {
                    $this->response->redirect('projects?id=' . $aRequestBody['project']);
                }
            }
            else
            {
                $this->response->redirect('');
            }
        }
        else
        {
            $this->response->redirect('');
        }
    }

    /**
     * Uplaod a image on AWS S3. the image is inserted on the member folder.
     * Imgix modified the url to return a responsive url
     */
    public function editonUploadImageAction()
    {
        $this->view->setRenderLevel(
            View::LEVEL_NO_RENDER
        );
        $aRequestBody = $this->request->getPost();
        if ($this->request->isPost())
        {
            $aRequestBody = $this->request->getPost();
            $aParameters = array();

            if ($this->request->hasFiles())
            {
                foreach ($this->request->getUploadedFiles() as $oFile)
                {
                    if ($oFile->isUploadedFile())
                    {
                        $sFileName = time() . '.' . $oFile->getExtension();

                        $sResult = $this->janksa_tools->uploadImages(
                            $oFile,
                            $this->session->get('member_id'),
                            $sFileName,
                            $this->aConfigurations['aws']['image_s3']
                        );

                        if (!empty($sResult))
                        {
                            $array = array(
                                'url' => $this->janksa_tools->getImageUrl($sResult, array('w' => 945, 'fit' => 'max', 'auto' => 'format')),
                                'id' => time()
                            );
                            echo stripslashes(json_encode($array));
                        }
                    }
                }
            }
        }
    }
}

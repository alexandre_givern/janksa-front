<?php
use Phalcon\Mvc\Controller;
use Phalcon\Http\Client\Request;
use Phalcon\Mvc\View;

class MessagesController extends Controller
{

    /**
     * Inbox page
     * Available only for a member connected. There are 2 possibilites :
     *
     * 1 - There are a message id on the HTTP header. The function verified the
     * member is correctly on the exchange. If it's true, all the messages of the exchange
     * are get and display.
     *
     * 2 - If there are not message id, we return the list of exchange associated
     * at the member
     */
    public function indexAction()
    {
        if ($this->session->has('member_id'))
        {
            if ($this->request->get('id'))
            {
                try {
                    $oResponse = $this->oProvider->get(
                        'discussions/' . $this->sApiToken . '/' . $this->request->get('id')
                    );
                    $aResponse = json_decode($oResponse->body, true);

                    if (empty($aResponse))
                    {
                        $this->response->redirect('messages/index');
                    }
                }
                catch (Exception $e)
                {
                    $this->response->redirect('messages/index');
                }

            }
            else
            {
                $aFilters = array(
                    'member' => $this->session->get('member_id')
                );

                try
                {
                    $oResponse = $this->oProvider->get(
                        'discussions/' . $this->sApiToken . '?filters=' . json_encode($aFilters)
                    );

                    $aResponse = json_decode($oResponse->body, true);
                }
                catch (Exception $e)
                {
                    $aResponse = array();
                }

                // We add the username with each messages
                if(!empty($aResponse))
                {
                    for ($i = 0, $c = count($aResponse); $i < $c; $i++)
                    {
                        if (!empty($aResponse[$i]['members']))
                        {
                            for ($ii = 0, $cc = count($aResponse[$i]['members']); $ii < $cc; $ii++)
                            {
                                $aParameters = array();
                                $aParameters['memberId'] = $aResponse[$i]['members'][$ii]['member'];
                                $aMember = MembersModel::getMember($aParameters);
                                $aResponse[$i]['members'][$ii]['username'] = $aMember['username'];
                            }
                        }

                    }
                    $this->view->aMessages = $aResponse;
                }
            }
        }
        else
        {
            $this->response->redirect('');
        }
    }

    /**
     * The member must to be connected to contact another member
     * The function can send the message to the other member with POST method.
     * Else we find the old exchange between the 2 members and we display a form
     * to send a message.
     */
    public function contactAction()
    {
        if ($this->session->has('member_id'))
        {
            if ($this->request->isPost())
            {
                $aRequestBody = $this->request->getPost();

                if (!empty($aRequestBody['discussion']) && !empty($aRequestBody['message']))
                {
                    $aParameters = array(
                        'member' =>  $this->session->get('member_id'),
                        'message' => $aRequestBody['message']
                    );

                    $this->oProvider->header->set('Content-Type', 'application/json');
                    $oResponse = $this->oProvider->put(
                        'discussions/' . $this->sApiToken . '/' . $aRequestBody['discussion'],
                        json_encode($aParameters)
                    );

                    $this->response->redirect('messages/contact?discussion=' . $aRequestBody['discussion']);
                }
                else
                {
                    $this->response->redirect('');
                }
            }
            else
            {
                if ($this->request->has('member') && !$this->request->has('discussion'))
                {
                    $aFilters = array(
                        'member' =>  $this->session->get('member_id'),
                        'secondMember' => $this->request->get('member')
                    );

                    $oResponse = $this->oProvider->get(
                        'discussions/' . $this->sApiToken . '?filters=' . json_encode($aFilters)
                    );

                    $aResponse = json_decode($oResponse->body, true);

                    if (empty($aResponse))
                    {
                        $aParameters = array();

                        $aParameters['members'] = array(
                            $this->request->get('member'),
                            $this->session->get('member_id')
                        );

                        $this->oProvider->header->set('Content-Type', 'application/json');
                        $oResponse = $this->oProvider->post(
                            'discussions/' . $this->sApiToken,
                            json_encode($aParameters)
                        );

                        $oResponse = $this->oProvider->get(
                            'discussions/' . $this->sApiToken . '?filters=' . json_encode($aFilters)
                        );

                        $aResponse = json_decode($oResponse->body, true);
                    }

                    $aParameters = array();
                    $oResponse = $this->oProvider->get(
                        'discussions/' . $this->sApiToken . '/' . $aResponse[0]['id'],
                        $aParameters
                    );

                    $aDiscussion = json_decode($oResponse->body, true);
                }
                elseif ($this->request->has('discussion'))
                {
                    $aParameters = array();
                    $oResponse = $this->oProvider->get(
                        'discussions/' . $this->sApiToken . '/' . $this->request->get('discussion'),
                        $aParameters
                    );

                    $aDiscussion = json_decode($oResponse->body, true);
                }
                else
                {
                    $this->response->redirect('');
                }

                if (empty($aDiscussion))
                {
                    $this->response->redirect('');
                }
                else
                {
                    for ($i = 0, $c = count($aDiscussion['members']); $i < $c; $i++)
                    {
                        $aParameters = array();
                        $aParameters['memberId'] = $aDiscussion['members'][$i]['member'];
                        $aMember = MembersModel::getMember($aParameters);
                        $aDiscussion['members'][$i]['username'] = $aMember['username'];
                        for ($ii = 0, $cc = count($aDiscussion['messages']); $ii < $cc; $ii++)
                        {
                            if ($aDiscussion['messages'][$ii]['member'] == $aDiscussion['members'][$i]['member'])
                            {
                                $aDiscussion['messages'][$ii]['username'] = $aMember['username'];
                            }
                        }
                    }
                    $this->view->aDiscussion = $aDiscussion;
                }

            }
        }
        else
        {
            $this->response->redirect('');
        }

    }
}

<?php
use Phalcon\Mvc\Model;
use Phalcon\Validation;
use Phalcon\Mvc\Model\Message;
use Phalcon\Validation\Validator\Email;
use Phalcon\Validation\Validator\PresenceOf;
use Phalcon\Validation\Validator\Uniqueness;

/**
 *
 * @author agivern
 *
 */
class Members extends Model
{
    public function initialize()
    {
        $this->setConnectionService('database-mbr');

        $this->setSource('members_mbr');

        $this->hasOne(
            'memberId',
            'member_informations_mbi',
            'memberId'
        );

        $this->hasMany(
            'memberId',
            'project_commentaries_prc',
            'memberId'
        );

        $this->hasMany(
            'memberId',
            'project_reviews_prr',
            'memberId'
        );

        $this->hasMany(
            'memberId',
            'project_members_prm',
            'memberId'
        );

        $this->useDynamicUpdate(true);
    }

    public function columnMap()
    {
        return array(
            'mbr_id' => 'memberId',
            'mbr_email' => 'email',
        );
    }
}

<?php
use Phalcon\Mvc\Model;
use Phalcon\Validation;
use Phalcon\Validation\Validator\PresenceOf;
use Phalcon\Mvc\Model\Message;


/**
 *
 * @author agivern
 *
 */
class MemberInformations extends Model
{

    public $mbi_fk_mbr_id;
    public $mbi_password;

    public function initialize()
    {
        $this->setConnectionService('database-mbr');

        $this->setSource('member_informations_mbi');

        $this->hasOne(
            'memberId',
            'members_mbr',
            'memberId'
        );

        $this->useDynamicUpdate(true);
    }

    public function columnMap()
    {
        return array(
            'mbi_fk_mbr_id' => 'memberId',
            'mbi_password' => 'password',
            'mbi_username' => 'username',
            'mbi_created_at' => 'createdAt'
        );
    }

}

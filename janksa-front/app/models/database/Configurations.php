<?php
use Phalcon\Mvc\Model;

/**
 *
 * @author agivern
 *
 */
class Configurations extends Model
{
    public function initialize()
    {
        $this->setConnectionService('database-cfg');

        $this->setSource('configurations_cfg');
    }

    public function columnMap()
    {
        return array(
            'cfg_id' => 'id',
            'cfg_name' => 'name',
            'cfg_config' => 'config',
        );
    }
}

<?php
use Phalcon\Validation;
use Phalcon\Validation\Validator\Email;
use Phalcon\Di as Di;

/**
 *
 * @author agivern
 *
 */
class MembersModel
{

/*****************************************************************************/
/******************************** P U B L I C ********************************/
/*****************************************************************************/

    /**
     * Control the datas before to insert the member
     *
     * @param mixed[] $array Array of the list of element to control
     *
     * @throws Exception 1/ insertMemberValidation()
     */
    public static function insertMember($aParameters)
    {
        try
        {
            self::insertMemberValidation($aParameters);

            $aParameters['password'] = Di::getDefault()->get('security')->hash($aParameters['password']);

            $oMember = new Members();
            if ($oMember->save($aParameters) == false)
            {
                return false;
            }

            $aParameters['memberId'] = $oMember->memberId;

            $oMemberInformations = new MemberInformations();
            if ($oMemberInformations->save($aParameters) == false)
            {
                $oMember->delete();
                return false;
            }
            else
            {
                return $oMember->memberId;
            }
        }
        catch (Exception $e)
        {
            throw $e;
        }
    }

    /**
     * Control the datas before to update the member
     *
     * @param mixed[] $array Array of the list of element to control
     *
     * @throws Exception 1/ insertMemberValidation()
     */
    public static function updateMember($aParameters)
    {
        try
        {
            self::updateMemberValidation($aParameters);

            if (isset($aParameters['password']))
            {
                $aParameters['password'] = Di::getDefault()->get('security')->hash($aParameters['password']);
            }

            $oMember = Members::findFirst(array(
                'memberId = ' . $aParameters['memberId']
            ));
            $oMember->save($aParameters);

            $oMemberInformations = MemberInformations::findFirst(array(
                'memberId = ' . $aParameters['memberId']
            ));
            $oMemberInformations->save($aParameters);
        }
        catch (Exception $e)
        {
            throw $e;
        }
    }

    /**
     * Control the datas before to update the member
     *
     * @param mixed[] $array Array of the list of element to control
     *
     * @throws Exception
     */
    public static function getMember($aParameters)
    {
        try
        {
            $aMember = array();
            $oMember = Members::findFirst(array(
                'memberId = ' . $aParameters['memberId']
            ));
            if ($oMember === false) return false;

            $aMember = $oMember->toArray();

            $oMemberInformations = MemberInformations::findFirst(array(
                'memberId = ' . $aMember['memberId']
            ));

            $aMember = array_merge($aMember, $oMemberInformations->toArray());

            return $aMember;
        }
        catch (Exception $e)
        {
            throw $e;
        }
    }

/*****************************************************************************/
/******************************* P R I V A T E *******************************/
/*****************************************************************************/

    /**
     * Control the datas
     *
     * @param mixed[] $array Array of the list of element to control
     *
     * @throws Exception 1/ emailValidation() exception
     *      2/ passwordValidation() exception
     */
    private static function insertMemberValidation($aParameters)
    {

        try {
            self::emailValidation($aParameters);
            self::passwordValidation($aParameters);
            self::usernameValidation($aParameters);
        }
        catch (Exception $e)
        {
            throw $e;
        }
    }

    /**
     * Control the datas and if the user can update a member
     *
     * @param mixed[] $array Array of the list of element to control
     *
     * @throws Exception 1/ emailValidation() 2/ passwordValidation()
     *      3/ If the user are not connected 4/ If the member id doesn't exist
     *      5/ If the user has not the right to edit the member id
     */
    private static function updateMemberValidation($aParameters)
    {
        try
        {
            if (!empty($aParameters['memberId']))
            {
                throw new Exception('The member\'s id is required');
            }

            $oMember = Members::findFirst(array(
                'memberId = ' . $aParameters['memberId']
            ));

            if (!$oMember)
            {
                throw new Exception('This member doesn\'t exist');
            }

            if ($aParameters['session_memberId'] != $aParameters['memberId'])
            {
                throw new Exception('This member can\'t be edited by you');
            }

            if (isset($aParameters['email']))
            {
                self::emailValidation($aParameters);
            }

            if (isset($aParameters['password']))
            {
                self::passwordValidation($aParameters);
            }

            if (isset($aParameters['username']))
            {
                self::usernameValidation($aParameters);
            }
        }
        catch (Exception $e)
        {
            throw $e;
        }
    }

    /**
     * Control the email
     *
     * @param mixed[] $array Array of the list of element to control
     *
     * @throws Exception 1/ If email is empty 2/ If email is already used
     *     3/ If email format is incorrect
     */
    private static function emailValidation($aParameters)
    {
        if (empty($aParameters['email']))
        {
            throw new Exception('Email is required');
        }

        $oValidation = new Validation();

        $oValidation->add(
            'email',
            new Email(['message' => ''])
        );

        $bValidation = $oValidation->validate($aParameters);

        if (!$bValidation)
        {
            throw new Exception('Email format is incorrect');
        }

        if (isset($aParameters['memberId']))
        {
            $oOtherMemberWithEmail = Members::findFirst(array(
                'conditions' => 'email = ?0 AND memberId != ?1',
                'bind' => array(
                    $aParameters['email'],
                    $aParameters['memberId']
                )
            ));
        }
        else
        {
            $oOtherMemberWithEmail = Members::findFirst(array(
                'conditions' => 'email = ?0',
                'bind' => array(
                    $aParameters['email']
                )
            ));
        }

        if ($oOtherMemberWithEmail)
        {
            throw new Exception('Email already exists');
        }
    }

    /**
     * Control the password and password confirmation
     *
     * @param mixed[] $array Array of the list of element to control
     *
     * @throws Exception 1/ If password or password confirmation are empty
     *     2/ If password and password confirmation are different
     */
    private static function passwordValidation($aParameters)
    {
        if (empty($aParameters['password']) || !isset($aParameters['passwordConfirmation']))
        {
            throw new Exception('Password and password\'s confirmation are required');
        }

        if (strcmp($aParameters['password'], $aParameters['passwordConfirmation']) !== 0)
        {
            throw new Exception('Password and password\'s confirmation are different');
        }
    }

    private static function usernameValidation($aParameters)
    {
        if (empty($aParameters['username']))
        {
            throw new Exception('Username is required');
        }

        if (isset($aParameters['memberId']))
        {
            $oOtherMemberWithUsername = MemberInformations::findFirst(array(
                'conditions' => 'username = ?0 AND memberId != ?1',
                'bind' => array(
                    $aParameters['username'],
                    $aParameters['memberId']
                )
            ));
        }
        else
        {
            $oOtherMemberWithUsername = MemberInformations::findFirst(array(
                'conditions' => 'username = ?0',
                'bind' => array(
                    $aParameters['username']
                )
            ));
        }

        if ($oOtherMemberWithUsername)
        {
            throw new Exception('Username already exists');
        }
    }
}

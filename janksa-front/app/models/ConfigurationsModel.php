<?php
use Phalcon\Di as Di;

/**
 *
 * @author agivern
 *
 */
class ConfigurationsModel
{

/*****************************************************************************/
/******************************** P U B L I C ********************************/
/*****************************************************************************/

    /**
     * Return a array with the configurations of the application
     */
    public static function getConfigurations()
    {
        try
        {
            $aConfigurations = array();
            $oConfigurations = Configurations::find();
            if ($oConfigurations !== false)
            {
                $aRequestConfigurations = $oConfigurations->toArray();

                for ($i = 0, $c = count($aRequestConfigurations); $i < $c; $i++)
                {
                    $aConfigurations[$aRequestConfigurations[$i]['name']] = json_decode(
                        $aRequestConfigurations[$i]['config'],
                        true
                    );
                }
            }
            return $aConfigurations;
        }
        catch (Exception $e)
        {
            throw $e;
        }
    }
}

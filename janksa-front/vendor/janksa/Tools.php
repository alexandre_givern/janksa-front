<?php
use Aws\S3\S3Client;
use Aws\S3\Exception\S3Exception;
use Imgix\UrlBuilder;

class Tools {

    private static $_instance = null;

    private function __construct() {
    }

    public static function getInstance() {
        if(is_null(self::$_instance)) {
            self::$_instance = new Tools();
        }

        return self::$_instance;
    }

    public function uploadImages($oFile, $iMemberId, $sFileName, $aConfigAws) {
        require '../vendor/aws/aws-autoloader.php';

        $sAccessKey = $aConfigAws['access_key'];
        $sSecretKey = $aConfigAws['secret_key'];
        $sRegion = $aConfigAws['region'];
        $sBucket = $aConfigAws['bucket'];
        $sFolder = $aConfigAws['folder'];
        $sMemberFolder = $iMemberId . '/';

        $s3 = S3Client::factory(
            array(
                'credentials' => array(
                    'key' => $sAccessKey,
                    'secret' => $sSecretKey
                ),
                'version' => 'latest',
                'region'  => $sRegion
            )
        );

        try {
            $sTmpFilePath = $_SERVER['DOCUMENT_ROOT'] . '/img/upload/' . $sFileName;
            $oFile->moveTo($sTmpFilePath);

            $result = $s3->putObject(
                array(
                    'Bucket' => $sBucket,
                    'Key' => $sFolder . 'upload/' . $sMemberFolder . $sFileName,
                    'SourceFile' => $sTmpFilePath,
                    'StorageClass' => 'REDUCED_REDUNDANCY',
                    'ACL' => 'public-read',
                )
            );

            return 'upload/' . $sMemberFolder . $sFileName;

        } catch (S3Exception $e) {
            echo $e->getMessage();
        } catch (Exception $e) {
            echo $e->getMessage();
        }

        return $result;
    }

    public function getImageUrl($sImagePath, $aParameters = array()) {
        $builder = new UrlBuilder('janksa.imgix.net', true);
        return $builder->createURL($sImagePath, $aParameters);
    }

    public function getImageTag($sImagePath, $aParameters = array(), $aTagParameters = array()) {
        $sTag = '<img';

        $sTag .= ' src="' . $this->getImageUrl($sImagePath, $aParameters) . '"';

        if (isset($aTagParameters['class']))
        {
            $sTag .= ' class="' . $aTagParameters['class'] .'"';
        }

        if (isset($aTagParameters['alt']))
        {
            $sTag .= ' alt="' . $aTagParameters['alt'] .'"';
        }

        $sTag .= ' />';

        return $sTag;
    }
}

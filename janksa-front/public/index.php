<?php
if ($_SERVER['APPLICATION_ENVIRONEMENT'] == 'development')
{
    ini_set('display_errors', 1);
    ini_set('display_startup_errors', 1);
    error_reporting(- 1);
}
else
{

}

use Phalcon\Loader;
use Phalcon\Mvc\View;
use Phalcon\Mvc\Application;
use Phalcon\DI\FactoryDefault;
use Phalcon\Session\Adapter\Files;
use Phalcon\Mvc\Url as UrlProvider;
use Phalcon\Db\Adapter\Pdo\Mysql as DbAdapter;
use Phalcon\Translate\Adapter\NativeArray;
use Phalcon\Http\Client\Request;
use Imgix\UrlBuilder;

require '../vendor/janksa/Tools.php';

$oLoader = new Loader();

$oLoader->registerDirs(
    array(
        '../app/controllers/',
        '../app/models/database',
        '../app/models'
    )
);

$oLoader->registerNamespaces(
    array(
        'Phalcon' => '../vendor/incubator/Library/Phalcon/',
        'Imgix' => '../vendor/imgix/Imgix/'
    )
);

$oLoader->register();

$oFactoryDefault = new FactoryDefault();
$oFactoryDefault->setShared('database-mbr', function () {
    $aDatabaseConfig = json_decode(stripslashes(stripslashes($_SERVER['APPLICATION_DB'])), true);
    $aDatabaseConfig['dbname'] = 'janksa_mbr';
    return new DbAdapter($aDatabaseConfig);
});

$oFactoryDefault->setShared('database-cfg', function () {
    $aDatabaseConfig = json_decode(stripslashes(stripslashes($_SERVER['APPLICATION_DB'])), true);
    $aDatabaseConfig['dbname'] = 'janksa_cfg';
    return new DbAdapter($aDatabaseConfig);
});

$oFactoryDefault->setShared('url', function() {
    $url = new UrlProvider();
    $url->setBaseUri('https://' . $_SERVER['HTTP_HOST'] . '/');
    return $url;
});

$oFactoryDefault->setShared('janksa_tools', function() {
    return Tools::getInstance();;
});

$oFactoryDefault->setShared('session', function() {
    $oSession = new Files();
    $oSession->start();
    return $oSession;
});

$oFactoryDefault->setShared('aConfigurations', function() {
    $aGlobalConfig = ConfigurationsModel::getConfigurations();
    return $aGlobalConfig;
});

$oFactoryDefault->setShared('oProvider', function() use($oFactoryDefault) {
    $oProvider = Request::getProvider();
    $oProvider->setBaseUri($oFactoryDefault->getShared('aConfigurations')['janksa-api']['url']);
    $oProvider->header->set('Accept', 'application/json');
    return $oProvider;
});

$oFactoryDefault->setShared('sApiToken', function() use($oFactoryDefault) {
    return sha1("H5812*Ub48J!" . date("dmY"));
});

$oFactoryDefault->setShared('sLanguage', function() use($oFactoryDefault) {
    $session = $oFactoryDefault->getShared('session');
    $request = $oFactoryDefault->getShared('request');

    if ($request->has('language'))
    {
        $session->set('language', $request->get('language'));
    }

    if ($session->has('language'))
    {
        $aLanguage = explode('-', $session->get('language'));
    }
    else
    {
        $aLanguage = explode('-', $request->getBestLanguage());
    }

    return $aLanguage[0];
});

$oFactoryDefault->setShared('translation', function() use($oFactoryDefault) {
    $sLanguage = $oFactoryDefault->getShared('sLanguage');

    if (file_exists('../app/languages/' . $sLanguage . '.php'))
    {
        require '../app/languages/' . $sLanguage . '.php';
    }
    else
    {
        require '../app/languages/fr.php';
    }
    return new NativeArray(array("content" => $messages));
});

$oFactoryDefault->set('view', function() use($oFactoryDefault) {
    $translation = $oFactoryDefault->getShared('translation');
    $sLanguage = $oFactoryDefault->getShared('sLanguage');
    $view = new View();
    $view->setViewsDir('../app/views/');
    $view->setVar('t', $translation);
    $view->setVar('sLanguage', $sLanguage);
    return $view;
});

$oApplication = new Application($oFactoryDefault);

try
{
    $response = $oApplication->handle();
    $response->send();
}
catch (Exception $e)
{
    header('Location: /');
}

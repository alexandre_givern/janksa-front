jQuery(document).ready(function(){

    jQuery.each(jQuery('.js-editor-project'), function(){
        jQuery(this).redactor({
            imageUpload: '/projects/editonUploadImage',
            imageResizable: true,
            imagePosition: false,
            imageCaption: false,
            formatting: ['p', 'h3', 'blockquote']
        });
    });

    jQuery('.js-project-tab').hide();
    jQuery('.js-project-presentation').show();
    jQuery('.js-project-presentation-button').addClass('c-project-view-nav__button--active');

    jQuery('.js-project-presentation-button').on('click', function(){
        jQuery('.js-project-tab').hide();
        jQuery('.js-project-presentation').show();
        jQuery('.js-project-tab-button').removeClass('c-project-view-nav__button--active');
        jQuery(this).addClass('c-project-view-nav__button--active');
    });

    jQuery('.js-project-comments-button').on('click', function(){
        jQuery('.js-project-tab').hide();
        jQuery('.js-project-comments').show();
        jQuery('.js-project-tab-button').removeClass('c-project-view-nav__button--active');
        jQuery(this).addClass('c-project-view-nav__button--active');
    });

    jQuery('.js-project-recruitment-button').on('click', function(){
        jQuery('.js-project-tab').hide();
        jQuery('.js-project-recruitment').show();
        jQuery('.js-project-tab-button').removeClass('c-project-view-nav__button--active');
        jQuery(this).addClass('c-project-view-nav__button--active');
    });

    jQuery('.js-project-news-button').on('click', function(){
        jQuery('.js-project-tab').hide();
        jQuery('.js-project-news').show();
        jQuery('.js-project-tab-button').removeClass('c-project-view-nav__button--active');
        jQuery(this).addClass('c-project-view-nav__button--active');
    });

    jQuery('.js-project-contact-button').on('click', function(){
        jQuery('.js-project-tab').hide();
        jQuery('.js-project-contact').show();
        jQuery('.js-project-tab-button').removeClass('c-project-view-nav__button--active');
        jQuery(this).addClass('c-project-view-nav__button--active');
    });
});

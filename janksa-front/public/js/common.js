var $window = jQuery(window);

function displayAlert(alertText, alertType) {
    var alertClone = jQuery('.js-callout').clone();
    alertClone.addClass(alertType);
    alertClone.show();
    alertClone.removeClass('js-callout');
    alertClone.find('.js-callout-text').text(alertText);
    alertClone.prependTo( ".js-callout-div" );
}

function submitForm(formTarget, successFunction = function(){}, errorFunction = function(){}) {
    jQuery.ajax({
        url : jQuery(formTarget).data('url'),
        type : jQuery(formTarget).attr('method'),
        data : jQuery(formTarget).serialize(),
        success : function(response, statut){
            var jsonResponse = jQuery.parseJSON(response);
            if (jsonResponse.error != undefined)
            {
                displayAlert(jsonResponse.error, 'alert');
                errorFunction();
            }
            else if (jsonResponse.success != undefined)
            {
                displayAlert(jsonResponse.success, 'success');
                successFunction();
            }
        }
    });
    return false;
}

jQuery(document).ready(function(){
    jQuery(document).foundation();

    // Signin and Signup button action to open popin
    jQuery('.js-button-sign').on('click', function(){
        jQuery('.js-menu-nav').css('left', '-300px');
        jQuery('.js-menu').css('background-color', 'rgba(0,0,0,0)');
        jQuery('.js-menu').css('z-index', '-1');
        jQuery('.js-open-menu-bars').show();
        jQuery('.js-open-menu-times').hide();

        jQuery('.js-account-menu-nav').css('right', '-300px');
        jQuery('.js-account-menu').css('background-color', 'rgba(0,0,0,0)');
        jQuery('.js-account-menu').css('z-index', '-1');

        if (jQuery('.js-popin-sign').html().length == 0)
        {
            jQuery.ajax('/members/sign')
            .done(function(resp){
                jQuery('.js-popin-sign').html(resp).foundation('open');
            });
        }
        else
        {
            jQuery('.js-popin-sign').foundation('open');
        }
    });

    // Open and close menu
    jQuery('.js-open-menu').on('click', function(){
        jQuery('.js-account-menu-nav').css('right', '-300px');
        jQuery('.js-account-menu').css('background-color', 'rgba(0,0,0,0)');
        jQuery('.js-account-menu').css('z-index', '-1');

        if (jQuery('.js-menu-nav').css('left') == '0px')
        {
            jQuery('.js-menu-nav').css('left', '-300px');
            jQuery('.js-menu').css('background-color', 'rgba(0,0,0,0)');
            jQuery('.js-menu').css('z-index', '-1');
            jQuery('.js-open-menu-bars').show();
            jQuery('.js-open-menu-times').hide();
        }
        else
        {
            jQuery('.js-menu-nav').css('left', '0px');
            jQuery('.js-menu').css('background-color', 'rgba(0,0,0,0.6)');
            jQuery('.js-menu').css('z-index', '101');
            jQuery('.js-open-menu-bars').hide();
            jQuery('.js-open-menu-times').show();
        }
    });

    jQuery('.js-close-menu').on('click', function(){
        jQuery('.js-menu-nav').css('left', '-300px');
        jQuery('.js-menu').css('background-color', 'rgba(0,0,0,0)');
        jQuery('.js-menu').css('z-index', '-1');
        jQuery('.js-open-menu-bars').show();
        jQuery('.js-open-menu-times').hide();
    });

    jQuery('.js-open-account-menu').on('click', function(){
        jQuery('.js-menu-nav').css('left', '-300px');
        jQuery('.js-menu').css('background-color', 'rgba(0,0,0,0)');
        jQuery('.js-menu').css('z-index', '-1');
        jQuery('.js-open-menu-bars').show();
        jQuery('.js-open-menu-times').hide();

        if (jQuery('.js-account-menu-nav').css('right') == '0px')
        {
            jQuery('.js-account-menu-nav').css('right', '-300px');
            jQuery('.js-account-menu').css('background-color', 'rgba(0,0,0,0)');
            jQuery('.js-account-menu').css('z-index', '-1');
        }
        else
        {
            jQuery('.js-account-menu-nav').css('right', '0px');
            jQuery('.js-account-menu').css('background-color', 'rgba(0,0,0,0.6)');
            jQuery('.js-account-menu').css('z-index', '101');
        }
    });

    jQuery('.js-close-account-menu').on('click', function(){
        jQuery('.js-account-menu-nav').css('right', '-300px');
        jQuery('.js-account-menu').css('background-color', 'rgba(0,0,0,0)');
        jQuery('.js-account-menu').css('z-index', '-1');
    });

    // Show element
    jQuery('.js-show-element').on('click', function(){
        var sElement = jQuery(this).data('element');

        jQuery('.' + sElement).show();
        jQuery('.' + sElement + '-hide').hide();
        jQuery(this).hide();
    });

    jQuery('.c-main-content').css(
        'min-height',
        $window.height() - 350
    );

    $window.resize(function() {
        if (jQuery('.c-main-content').length == 1 && $window.height() > jQuery('.c-main-content').height())
        {
            jQuery('.c-main-content').css(
                'min-height',
                $window.height() - 350
            );
        }
    });

    /**
     * Listener for every links clicked to send the data at Analytics
     *
     * Analytics tracking, the element should have data-category, data-action and data-label
     */
    jQuery(document).on('click', '.js-tracking-click', function(){
        if (typeof jQuery(this).data('category') != 'undefined' &&
            typeof jQuery(this).data('action') != 'undefined' &&
            typeof jQuery(this).data('label') != 'undefined' &&
            typeof dataLayer != 'undefined') {

            dataLayer.push({
                event: 'trackEvent',
                'trackEvent.category': jQuery(this).data('category'),
                'trackEvent.action': jQuery(this).data('action'),
                'trackEvent.label': jQuery(this).data('label'),
            });
        }
        else {}
    });

    // Diplay the button to go top directly by one click
    window.onscroll = function() {
        if (document.body.scrollTop > 20 || document.documentElement.scrollTop > 20) {
            jQuery('.js-up').css('display', 'block');
        } else {
            jQuery('.js-up').css('display', 'none');
        }
    };
    jQuery('.js-up').click(function(){
        jQuery('html, body').animate({scrollTop : 0},500);
    });
});

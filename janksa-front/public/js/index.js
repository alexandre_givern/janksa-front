jQuery(document).ready(function(){

    if (($window.width() / parseFloat($("body").css("font-size"))) < 64)
    {
        jQuery.each(jQuery('.js-index-what__icon'), function() {
            jQuery(this).css(
                'line-height',
                jQuery(this).next('.js-index-what__text').height() + 25 + 'px'
            );
            jQuery(this).css(
                'height',
                jQuery(this).next('.js-index-what__text').height()
            );
        });
    }

    $window.resize(function() {
        if (($window.width() / parseFloat(jQuery('body').css('font-size'))) < 64)
        {
            jQuery.each(jQuery('.js-index-what__icon'), function() {
                jQuery(this).css(
                    'line-height',
                    jQuery(this).next('.js-index-what__text').height() + 25 + 'px'
                );
                jQuery(this).css(
                    'height',
                    jQuery(this).next('.js-index-what__text').height()
                );
            });
        }
        else
        {
            jQuery.each(jQuery('.js-index-what__icon'), function() {
                jQuery(this).css(
                    'line-height',
                    '0'
                );
                jQuery(this).css(
                    'height',
                    'auto'
                );
            });
        }
    });

});

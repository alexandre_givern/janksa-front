jQuery(document).ready(function(){

    jQuery.each(jQuery('.js-editor-article'), function(){
        jQuery(this).redactor({
            imageUpload: '/articles/uploadImage',
            imageResizable: true,
            imagePosition: false,
            imageCaption: false
        });
    });
});
